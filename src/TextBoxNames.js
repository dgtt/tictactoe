import React from 'react';
import TextBoxName from './TextBoxName.js';

class TextBoxNames extends React.Component {


	handleChange(value, index) {
		let users = this.props.users;
		users[index] = value;
		this.props.updateUpdate(users);
	}

	render() {
		
		return (
			<div>
				{this.props.users.map((item, index) => 
					<TextBoxName key={index} no={index} handleChange={(value, index) => this.handleChange(value, index)} />	
				)}
			</div>
		);

	}
}

export default TextBoxNames