import React from 'react';

class Result extends React.Component {

	constructor(props) {
		super(props);
		
		this.state = {
			users: [],
			xIsNext: true,
			result: null
		};
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			users: nextProps.users,
			xIsNext: nextProps.xIsNext,
			result: nextProps.result
		});
	}

  	render() {
  		let result;

  		if (this.state.result === true) {
	      	result = <div className="draw">Draw !!</div>;
	    } else if(this.state.result === null) {
	    	let nextNoUser = (this.state.xIsNext) ? 0 : 1;
  			let nextPlayerName = this.state.users[nextNoUser];
  			let sign = this.state.xIsNext ? 'X' : 'O';
  			nextPlayerName = nextPlayerName ? nextPlayerName : sign;
			let userClass = 'user user-' + sign;
	      	result = <div className="skip">Next player: <span className={userClass}>{nextPlayerName}</span></div>;
	    }else {
	    	let winnerNoUser = (!this.state.xIsNext) ? 0 : 1;
	    	let sign = (!this.state.xIsNext ? 'X' : 'O');
      		let winnerName = this.state.users[winnerNoUser];
      		winnerName = winnerName ? winnerName : sign;
      		let userClass = 'user user-' + sign;
	      	result = <div className="win"><span className={userClass}>{winnerName}</span> Win !!</div>;
	    }
    	return (
	      	<div>{result}</div>
    	);
  	}
}

export default Result