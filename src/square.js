import React from 'react';

class Square extends React.Component {

	constructor(props) {
		super(props);
		
		this.state = {
			className: 'square'
		};
	}

	componentWillReceiveProps(nextProps) {
		let className = (nextProps.value) ? 'square square-' + nextProps.value : 'square';
		this.setState({
			className: className
		});
	}

  	render() {
    	return (
      	<button className={this.state.className} onClick = {() => this.props.onClick()}>
        	{this.props.value}
      	</button>
    	);
  	}
}

export default Square