import React from 'react';
import Board from './board.js';
import History from './history.js';
import TextBoxNames from './TextBoxNames.js';
import Result from './result.js';

class Game extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			history: [{
		        squares: Array(9).fill(null)
		     }],
			xIsNext: true,
			stepNumber: 0,
			users: ["", ""]
		};
	}

	handleClick(i) {
		  let history = this.state.history.slice(0, this.state.stepNumber + 1);
		  const current = history[history.length - 1];
	    const squares = current.squares.slice();

	    if (calculateWinner(squares) !== null || squares[i]) {
	      return;
	    }

	    squares[i] = this.state.xIsNext ? 'X' : 'O';

	    this.setState({
	    	history: history.concat([{
		        squares: squares
		    }]),
	    	xIsNext: !this.state.xIsNext,
	    	stepNumber: history.length
	    });
	}

	jumpTo(stepNumber) {
    let history = this.state.history.slice(0, stepNumber + 1);
		this.setState({
        history: history,
      	stepNumber: stepNumber,
     		xIsNext: (stepNumber % 2) === 0
    })
	}

	updateUsers(users) {
		this.setState({
			users: users
		});
	}

  render() {
  	const history = this.state.history;
    const current = history[this.state.stepNumber];
    const result = calculateWinner(current.squares);

    return (
      <div className="game">
        <div className="game-board">
           	<Board
            	squares={current.squares}
            	onClick={(i) => this.handleClick(i)}
          	/>
        </div>
        <div className="game-info">
        	<TextBoxNames users={this.state.users} updateUpdate={(users) => this.updateUsers(users)} />
          	<Result users={this.state.users} xIsNext={this.state.xIsNext} result={result}  />
          	<History 
	          	history={this.state.history} 
	          	onClick={(move) => this.jumpTo(move)}
          	/>
        </div>
      </div>
    );
  }
}

export default Game;

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }

  if( isDraw(squares) ) {
    return true;
  }

  return null;
}

function isDraw(squares) {
  let isFinishGame = true;
	for (let i = 0; i < squares.length; i++) {
  		if ( squares[i] === null ) {
  			isFinishGame = false;
  		}
  	}
  	return isFinishGame;
}
