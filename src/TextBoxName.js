import React from 'react';

class TextBoxName extends React.Component {
	
	handleChange(e) {
		this.props.handleChange(e.target.value, this.props.no);
	}

	render() {

		let sign = (!this.props.no) ? 'X':'O';
		let labelClass = 'label label-' + sign;
		let label = <label className={labelClass}>{sign}</label>;
		return (
			<div>{label}<input type="text" onChange={(e) => this.handleChange(e)} /></div>
			
		);

	}
}

export default TextBoxName